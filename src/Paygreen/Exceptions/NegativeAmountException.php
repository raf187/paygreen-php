<?php

namespace Oprax\Paygreen\Exceptions;

class NegativeAmountException extends PaygreenException {}
