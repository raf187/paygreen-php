<?php

namespace Oprax\Paygreen\Exceptions;

class NotSucceedException extends PaygreenException {
    /**
     * @var string
     */
    private $url;

    public function __construct($message = "", $url = null)
    {
        parent::__construct($message);
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
